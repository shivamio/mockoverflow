## Installation

Clone the repository

```bash
git clone https://bitbucket.org/shivamio/mockoverflow.git
```

Install

```bash
npm install
```

Run

```bash
npm start
```

## Usage

* Registering user (POST)

url
```bash
http://localhost:8000/users/register
```
body
```bash
{
    "name":"Rahul Dravid",
    "username":"rahul007",
    "password":"SuperSecret"
}
```
response
```bash
{
    "success":true,
    "msg":"Registration Complete"
}
```

* Login user (POST)

url
```bash
http://localhost:8000/users/login
```
body
```bash
{
    "username":"rahul007",
    "password":"SuperSecret"
}
```
response
```bash
{
    "success":true,
    "msg":"logged in",
    "authToken":token,
    "user":{
        "name":"Rahul Dravid",
        "username":"rahul007"
    }
}
```

* Asking Question (POST)
url
```bash
http://localhost:8000/questions/ask
```
headers
```bash
{
    "Content-Type":"application/json",
    "authKey":"auth_token_returned_while_login"
}
```
body
```bash
{
    "tag":"java",
    "title":"Using JWT in Spring boot",
    "description":"How can I implement JWT authentication in my Spring boot app"
}
```
response
```bash
{
    "success":true,
    "msg":"Question posted"
}
```

* Answering Question (POST)
url
```bash
http://localhost:8000/questions/answer/{questionid}
```
headers
```bash
{
    "Content-Type":"application/json",
    "authKey":"auth_token_returned_while_login"
}
```
body
```bash
{
    "answer":"This is a stupid question and should be marked duplicate :)"
}
```
response
```bash
{
    "success":true,
    "msg":"Answer posted successfully"
}
```

Other important endpoints

add new skill
```bash
http://localhost:8000/users/addskills
```

list all questions (that match user's skills)
```bash
http://localhost:8000/questions
```

list my asked questions
```bash
http://localhost:8000/questions/myquestions
```

delete a question (You can only delete your asked questions)
```bash
http://localhost:8000/questions/delete/{questionid}
```