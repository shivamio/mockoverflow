const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

const config = require('./config/database');

mongoose.connect(config.database,{
    useNewUrlParser:true
});

mongoose.connection.on('connected',()=>{
    console.log('Connected to database: '+config.database);
});
mongoose.connection.on('error', (err) => {
    console.log('Error occured while connecting to database: ' + err);
});

const app = express();
const port = 8000;

app.use(cors());
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname,'public')));

const users = require('./routes/users');
const questions = require('./routes/questions');

//Routes start
app.use('/users',users);
app.use('/questions', questions);
//Routes end

var server = app.listen(port, ()=>{
    console.log("Server started on Port: "+port)
})
