const express = require('express');
const router = express.Router();
const User = require('../models/user');

router.get('/', (req,res,next)=>{
    const authToken = req.header('authToken');
    if(authToken == undefined || authToken == ''){
        res.status(401).json({
            success:false,
            msg:'Unauthorized'
        })
    }else{
        User.getUserWithToken(authToken, (err,user)=>{
            if(err){
                res.json({
                    success:false,
                    msg:'Internal Server Error'
                })
            }
            if(user){
                res.json({
                    success:true,
                    msg:'User fetched',
                    user:{
                        name:user.name,
                        username:user.username,
                        skills:user.skills
                    }
                })
            }else{
                res.status(401).json({
                    success:false,
                    msg:'Unauthorized'
                })
            }
        })
    }
})

router.post('/register', (req,res,next)=>{
    const newUser = new User({
        name:req.body.name,
        username:req.body.username,
        password:req.body.password
    })
    User.getUserByUsername(req.body.username, (err, isThere)=>{
        if(err){
            res.json({
                success:false,
                msg:'Internal Server Error'
            })
        }
        if(isThere){
            res.json({
                success:false,
                msg:'Username not available'
            })
        }else{
            User.createUser(newUser, (err2, created)=>{
                if(err2){
                    res.json({
                        success:false,
                        msg:'Internal Server Error'
                    })
                }
                if(created){
                    res.json({
                        success:true,
                        msg:'Registration Complete'
                    })
                }else{
                    res.json({
                        success:false,
                        msg:'Failed to Register'
                    })
                }
            })
        }
    })
})

router.post('/login', (req,res,next)=>{
    const username = req.body.username;
    const password = req.body.password;

    User.getUserByUsername(username, (err, isThere)=>{
        if(err){
            res.json({
                success:false,
                msg:'Internal Server Error'
            })
        }
        if(isThere){
            User.comparePassword(password, isThere.password, (err2, correct)=>{
                if(err2){
                    res.json({
                        success:false,
                        msg:'Internal Server Error'
                    })
                }
                if(correct){
                    //return token
                    res.json({
                        success:true,
                        msg:'logged in',
                        token:isThere.authToken,
                        user:{
                            name:isThere.name,
                            username:isThere.username
                        }
                    })
                }else{
                    res.json({
                        success:false,
                        msg:'Invalid credentials'
                    })
                }
            })
        }else{
            //User not registered
            res.json({
                success:false,
                msg:'Invalid credentials'
            })
        }
    })

})

router.post('/addskills', (req,res,next)=>{
    const authToken = req.header('authToken');
    if(authToken == undefined || authToken == ''){
        res.status(401).json({
            success:false,
            msg:'Unauthorized'
        })
    }else{
        User.getUserWithToken(authToken, (err,user)=>{
            if(err){
                console.log(err);
                res.json({
                    success:false,
                    msg:'Internal Server Error'
                })
            }
            if(user){
                const newskills = req.body.newskills;
                User.addNewSkills(user.username, newskills, (err2, done)=>{
                    if(err2){
                        console.log(err2);
                        res.json({
                            success:false,
                            msg:'Internal Server Error'
                        })
                    }
                    if(done){
                        res.json({
                            success:true,
                            msg:'New Skills added'
                        })
                    }else{
                        res.json({
                            success:false,
                            msg:'Failed to add skills'
                        })
                    }
                })
            }else{
                res.status(401).json({
                    success:false,
                    msg:'Unauthorized'
                })
            }
        })
    }
})


module.exports = router;