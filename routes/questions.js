const express = require('express');
const router = express.Router();
const Question = require('../models/question');
const User = require('../models/user');

router.get('/myquestions', (req,res,next)=>{
    const authToken = req.header('authToken');
    if(authToken == undefined || authToken == ''){
        res.status(401).json({
            success:false,
            msg:'Unauthorized'
        })
    }else{
        User.getUserWithToken(authToken, (err,user)=>{
            if(err){
                res.json({
                    success:false,
                    msg:'Internal Server Error'
                })
            }
            if(user){
                const username = user.username;
                Question.listMyQuestions(username, (err2, questions)=>{
                    if(err2){
                        res.json({
                            success:false,
                            msg:'Internal Error Occured'
                        })
                    }
                    if(questions){
                        res.json({
                            success:true,
                            msg:'Questions fetched',
                            questions:questions
                        })
                    }else{
                        res.json({
                            success:false,
                            msg:'Failed to fetch questions'
                        })   
                    }
                })
            }else{
                res.status(401).json({
                    success:false,
                    msg:'Unauthorized'
                })
            }
        })
    }
})

router.get('/', (req,res,next)=>{
    const authToken = req.header('authToken');
    if(authToken == undefined || authToken == ''){
        res.status(401).json({
            success:false,
            msg:'Unauthorized'
        })
    }else{
        User.getUserWithToken(authToken, (err,user)=>{
            if(err){
                res.json({
                    success:false,
                    msg:'Internal Server Error'
                })
            }
            if(user){
                const skills = user.skills;
                Question.listBySkills(skills, (err2, questions)=>{
                    if(err2){
                        res.json({
                            success:false,
                            msg:'Internal Error Occured'
                        })
                    }
                    if(questions){
                        res.json({
                            success:true,
                            msg:'Questions fetched',
                            questions:questions
                        })
                    }else{
                        res.json({
                            success:false,
                            msg:'Failed to fetch questions'
                        })   
                    }
                })
            }else{
                res.status(401).json({
                    success:false,
                    msg:'Unauthorized'
                })
            }
        })
    }
})

router.post('/ask', (req,res,next)=>{
    const authToken = req.header('authToken');
    if(authToken == undefined || authToken == ''){
        res.status(401).json({
            success:false,
            msg:'Unauthorized'
        })
    }else{
        User.getUserWithToken(authToken, (err,user)=>{
            if(err){
                res.json({
                    success:false,
                    msg:'Internal Server Error'
                })
            }
            if(user){
                const askedBy = user.username;
                const newQuestion = new Question({
                    askedBy:askedBy,
                    tag:req.body.tag,
                    title:req.body.title,
                    description:req.body.description
                })
                Question.newQuestion(newQuestion, (err2, done)=>{
                    if(err2){
                        res.json({
                            success:false,
                            msg:'Internal Server Error'
                        })
                    }
                    if(done){
                        res.json({
                            success:true,
                            msg:'Question posted successfully'
                        })
                    }else{
                        res.json({
                            success:false,
                            msg:'Failed to post question'
                        })
                    }
                })
            }else{
                res.status(401).json({
                    success:false,
                    msg:'Unauthorized'
                })
            }
        })
    }
})

router.get('/delete/:questionid', (req,res,next)=>{
    const authToken = req.header('authToken');
    const questionid = req.params.questionid;
    if(authToken == undefined || authToken == ''){
        res.status(401).json({
            success:false,
            msg:'Unauthorized'
        })
    }else{
        User.getUserWithToken(authToken, (err,user)=>{
            if(err){
                res.json({
                    success:false,
                    msg:'Internal Server Error'
                })
            }
            if(user){
                if(questionid == undefined || questionid == ''){
                    res.json({
                        success:false,
                        msg:'Question id required'
                    })
                }else{
                    Question.getQuestion(questionid, (err2, thisQuestion)=>{
                        if(err2){
                            res.json({
                                success:false,
                                msg:'Internal Server Error'
                            })
                        }
                        if(thisQuestion){
                            const username = user.username;
                            const questionAsker = thisQuestion.askedBy;
                            if(questionAsker == username){
                                Question.deleteQuestion(questionid, (err3, deleted)=>{
                                    if(err3){
                                        res.json({
                                            success:false,
                                            msg:'Internal Error Occured'
                                        })
                                    }
                                    if(deleted){
                                        res.json({
                                            success:true,
                                            msg:'Question deleted'
                                        })
                                    }else{
                                        res.json({
                                            success:false,
                                            msg:'Failed to delete question'
                                        })
                                    }
                                })
                            }else{
                                res.json({
                                    success:false,
                                    msg:'Action not permitted'
                                })
                            }

                        }else{
                            res.json({
                                success:false,
                                msg:'Invalid Question id'
                            })
                        }
                    })
                }
            }else{
                res.status(401).json({
                    success:false,
                    msg:'Unauthorized'
                })
            }
        })
    }
})

router.post('/answer/:questionid', (req,res,next)=>{
    const questionid = req.params.questionid;
    const authToken = req.header('authToken');
    if(authToken == undefined || authToken == ''){
        res.status(401).json({
            success:false,
            msg:'Unauthorized'
        })
    }else{
        User.getUserWithToken(authToken, (err,user)=>{
            if(err){
                res.json({
                    success:false,
                    msg:'Internal Server Error'
                })
            }
            if(user){
                if(questionid == undefined || questionid == ''){
                    res.json({
                        success:false,
                        msg:'Question id required'
                    })
                }else{
                    Question.getQuestion(questionid, (err2, thisQuestion)=>{
                        if(err2){
                            res.json({
                                success:false,
                                msg:'Internal Error occured'
                            })
                        }
                        if(thisQuestion){
                            const username = user.username;
                            const answer = req.body.answer;
                            Question.answerQuestion(questionid, username, answer, (err3, answered)=>{
                                if(err3){
                                    res.json({
                                        success:false,
                                        msg:'Internal Server Error'
                                    })
                                }
                                if(answered){
                                    res.json({
                                        success:true,
                                        msg:'Answer posted successfully'
                                    })
                                }else{
                                    res.json({
                                        success:false,
                                        msg:'Failed to post answer'
                                    })
                                }
                            })
                        }else{
                            res.json({
                                success:false,
                                msg:'Invalid Question id'
                            })
                        }
                    })
                }
            }else{
                res.status(401).json({
                    success:false,
                    msg:'Unauthorized'
                })
            }
        })
    }
})


module.exports = router;