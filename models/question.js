const mongoose = require('mongoose');

const QuestionSchema = mongoose.Schema({
    askedBy:{
        type:String,
        required:true
    },
    tag:{
        type:String,
        required:true
    },
    title:{
        type:String,
        required:true
    },
    description:{
        type:String,
        required:true
    },
    isAnswered:{
        type:Boolean,
        default:false
    },
    totalAnswers:{
        type:Number,
        default:0
    },
    answers:[
        {
            username:String,
            ans:String
        }
    ]
})

const Question = module.exports = mongoose.model('Question', QuestionSchema);

module.exports.newQuestion = function(newQues, callback){
    newQues.save(callback);
}

module.exports.answerQuestion = function(quesId, username, ans, callback){
    const ansObj = {
        username:username,
        ans:ans
    }
    Question.findById(quesId, (err, question)=>{
        if(err){
            return callback(err, false);
        }
        if(question){
            var totalAns = question.totalAnswers
            var newTotal = totalAns+1;
            Question.findByIdAndUpdate(quesId, {isAnswered:true, totalAnswers:newTotal, $push:{answers:ansObj}}, callback)
        }else{
            return callback(null, false);
        }
    })
}

module.exports.listBySkills = function(skillList, callback){
    Question.find({tag:{$in:skillList}},callback);
}

module.exports.listMyQuestions = function(username, callback){
    Question.find({askedBy:username}, callback);
}

module.exports.deleteQuestion = function(quesId, callback){
    Question.findByIdAndDelete(quesId, callback);
}

module.exports.getQuestion = function(quesId, callback){
    Question.findById(quesId, callback);
}