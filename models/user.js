const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const random = require('randomstring');

const UserSchema = mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    username:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    skills:[
        {type:String}
    ],
    authToken:{
        type:String,
        required:true
    }
})

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.createUser = function(newUser, callback){
    bcrypt.genSalt(10, (err, salt)=>{
        bcrypt.hash(newUser.password, salt, (err2, hash)=>{
            newUser.password = hash;
            newUser.authToken = random.generate(32);
            newUser.save(callback);
        })
    })
}

module.exports.getUserByUsername = function(username, callback){
    User.findOne({username:username},callback);
}

module.exports.comparePassword = function(inputPass, hash, callback){
    bcrypt.compare(inputPass,hash,(err, isMatch)=>{
        if(err){
            callback(err,false);
        }else{
            callback(null,isMatch);
        }
    })
}

module.exports.getUserWithToken = function(token, callback){
    User.findOne({authToken:token},callback);
}

module.exports.addNewSkills = function(username, skills, callback){
    User.findOneAndUpdate({username:username}, {$push:{skills:skills}}, callback);
}